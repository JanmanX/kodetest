﻿using System;
using System.Collections.Generic;

namespace VacationCost
{
	public class VacationCostCalculator
	{
		// Dictionary of the transport methods, and their cost multiplier
		private Dictionary<string, int> transportMethods = new Dictionary<string, int> ();

		// Distance travelled
		public double DistanceToDestination { get; set; }


		public VacationCostCalculator ()
		{
			// Create initial transport methods
			transportMethods.Add ("Car", 1);
			transportMethods.Add ("Plane", 2);
		}

		public void AddTransportMethod (string name, int cost)
		{
			// Test if transport method exists. 
			// If exist, update cost, else add new entry
			if (transportMethods.ContainsKey (name)) {
				transportMethods [name] = cost;
			} else {
				transportMethods.Add (name, cost);
			}


		}

		public decimal CostOfVacation (string transportMethod)
		{
			// Lookup the transportMethod, and apply its cost multiplier
			if (transportMethods.ContainsKey (transportMethod)) {
				return (decimal)(DistanceToDestination * transportMethods [transportMethod]);
			} else {
				throw new ArgumentOutOfRangeException ();
			}
		}
	}
}
