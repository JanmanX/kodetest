# Design a database

Construct a schema that solves the following problem.

> Design a schema for a web shop. The shop has an inventory of products, each item has a price but depending on sales this price can vary.
> Customers can make an order for multiple items at a time and want to be able to see their order history as it was at the time of ordering.
> When the order has been completed an optional track and trace number can be attached to the package.

Make it simple to work with while also keeping performance in mind.

# Solution

If you're using Visual Studio you can use the ADO.NET Entity Data Model Designer (open Model1.edmx in the included project) to model your schema.

If you can't use the ADO.NET Entity Data Model Designer then use a UML tool of your choice and include a picture of your model.
It's very important that you include table cardinality.

## What you'll be evaluated on

You'll be evaluated on your ability to correctly design a schema that solves the problem.



SOLUTION:

For a simple webshop, each user is identified with a Name and an Address. Each
user also has en unique ID, although that is usually not visible outside the 
system.
When a user places an order, he gets an unique Order ID (ID_Order), which he 
can use to view the order. In the Order table, ID_Order is an unique ID, with
the customer ID and an optional Track&Trace number.
To view what has been ordered, the system will use the ID_Order to lookup the 
ID in the Order-to-Items table. Each order can have multiple entries in the table,
according to the number of different items ordered. The items are distinguished
by their ID, which points to the Items table, which has all the items stored.
To keep track of items no longer available, and previous prices, the 'Available'
filed in the table marks whether the item is still available or not.


I star (*) marks the unique identifier in each table.




                                   Customer table
                                   +--------------+------+------------------+
                             +---- |*ID_Customer  | Name | Address          |
                             |     +----------------------------------------+
                             |     |              |      |                  |
                             |     |              |      |                  |
                             |     |              |      |                  |
                             |     +--------------+------+------------------+
                             |
                             |
                             |
                             v
    Order table                                              Items table
    +---------------+---------------+-------------+          +------------+------+-------+--------------+
+-- | *ID_Order     |   ID_Customer | Track&Trace |       +- |*ID_Item    | Name | Price | Available    |
|   +---------------------------------------------+       |  +------------------------------------------+
|   |               |               |             |       |  |            |      |       |              |
|   |               |               |             |       |  |            |      |       |              |
|   |               |               |             |       |  |            |      |       |              |
|   +---------------+---------------+-------------+       |  +------------+------+-------+--------------+
|                                                         |
|                                                         |
|                                                         |
|                                                         |
|                                                         v
|                                 Order_to_Items table
|                                 +--------------+---------------+-------------------+
+-------------------------------> |  ID_Order    |   ID_Item     |    Quantity       |
                                  +--------------------------------------------------+
                                  |              |               |                   |
                                  |              |               |                   |
                                  |              |               |                   |
                                  +--------------+---------------+-------------------+


