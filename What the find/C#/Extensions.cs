﻿using System;
using System.Collections.Generic;

namespace WhatTheFind
{
    public static class Extensions
    {
        /// <summary>
        /// Find any node in an object graph that satisfy a given predicate and return it.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="root">The root node.</param>
        /// <param name="predicate">The given condition to satisfy.</param>
        /// <param name="getChildren">Child selector.</param>
        /// <returns>Node satisfying the condtion, else null.</returns>
        public static T FindWhere<T>(this T root, Func<T, bool> predicate, Func<T, IEnumerable<T>> getChildren)
            where T : class
        {
            // YOUR SOLUTION GOES HERE
			//
			// In this solution, I apply Depth-first search (DFS), by
			// first checking the root node, and then recursivelly calling
			// on each child in the tree.
			//
			// In this solution, I assume that the tree does not contain any
			// recursive nodes, in which case, this algorithm will be stuck in
			// endless loop.
			// If this was indeed the case, a list of "visited" nodes would have 
			// to be created.
	

			// Check the predicate on the root node
			if (predicate (root)) {
				return root;
			}

			// Recursivelly call on all children
			foreach (T child in getChildren(root)) {
				T ret = FindWhere (child, predicate, getChildren);
				if (ret != null) {
					return ret;
				}
			}

			// Nothing found
			return null;
        }
    }
}