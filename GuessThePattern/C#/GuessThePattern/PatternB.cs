﻿namespace GuessThePattern
{
    public abstract class SomeObject
    {
    }

    public class ConcreteSomeObject : SomeObject
    {
    }

    public abstract class PatternB
    {
        public abstract SomeObject Create();
    }

    public class ConcretePatternB : PatternB
    {
        public override SomeObject Create()
        {
            return new ConcreteSomeObject();
        }
    }
}

/* Write your answers and comments below this line

Here, the class ConcretePatternB makes sure the correctly
instantiate object ConcreteSomeObject for us. This can be 
helpful, if a lot of complexity goes into creating such
objects.
My guess is that this pattern is the Factory model, which
has a factory class. 

*/
