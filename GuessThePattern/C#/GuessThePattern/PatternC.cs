﻿using System.Collections.Generic;

namespace GuessThePattern
{
    public abstract class Foo
    {
        private readonly List<Bar> _list = new List<Bar>();

        public void Attach(Bar bar)
        {
            _list.Add(bar);
        }

        public void Detach(Bar bar)
        {
            _list.Remove(bar);
        }

        public void Notify()
        {
            foreach (var o in _list)
            {
                o.Update();
            }
        }
    }

    public abstract class Bar
    {
        protected readonly Foo Foo;

        protected Bar(Foo foo)
        {
            Foo = foo;
        }

        public abstract void Update();
    }
}

/* Write your answers and comments below this line

I am not familiar with the name or the designs of 
this patteren, although I have seen it heavily used in many
contexts.

For example, in an UI application, the "Foo" can represent
the Window, while "Bar" can be all Window buttons, widgets, and
other decoration. 
In a time interval, the Window might wish to update all its 
contents, calling "Notify", which in turn calls Update() method
on all its children.

*/
