﻿using System;

namespace GuessThePattern
{
    public class PatternA
    {
        private static PatternA _instance;
        private PatternA()
        {
        }

        public static PatternA GetInstance()
        {
            if (_instance == null)
                _instance = new PatternA();

            return _instance;
        }

        public void DoWork()
        {
            throw new NotImplementedException();
        }
    }
}

/* Write your answers and comments below this line

Here, the class PatternA makes sure that it is instantiated,
and returns that object. Thus, there can only be one instace
of PatternA at a time.
My guess is that this pattern is the "Singleton" pattern.

The advantage of using a singleton class is the ability to
use it without having to deal with instantiation (and eventual
destruction).
For example, a Math singleton class can expose a variety of functions,
such as cos(), sin(), tan(). Since these calculations are not
specific in any way, there is no need to instantiate this object
for each use. This saves both time and memory.

The disadvantage of static (singleton) classes is that it is impossible
to derive from. Using the above example, if we wanted to use degrees
in our trigonometric functions instead of radians, we cannot simply
derive the class, to change those methods.
*/
