﻿using System;

namespace Graf
{
    public class Finder : IFinder
    {
        public string FromRight(Customers customers, int numberFromRight)
        {
            // YOUR SOLUTION GOES HERE
			//
			// In my solution, I first iterate through the list to finds its length,
			// and then I find the 'index' of the correct memeber, and iterate to it 
			// again. This uses at most O(n) iterations.
			//
			// After implementation, it occured to me that it would be possible to have
			// 2 pointers, with 'numberFromRight'-distance between them. When the first
			// pointer would reach 'null', the second pointer would point to the correct
			// element.


			// Check for valid arguments
			if (customers == null) {
				throw new ArgumentNullException ("customers cannot be null!");
			}
			if (numberFromRight < 1) {
				throw new ArgumentOutOfRangeException ("numberFromRight is out of (reachable) index!");
			}

			// Calculate length of customers 
			int length = 0;
			for (Customers c = customers; c != null; c = c.Next) {
				length++;
			}

			// Calculate number of jumps to be done by subtracting the 
			// total length by the numberFromRight. This will get us the number
			// of jumps needed to be done, to reach the 'numberFromRight' customer.
			// Throw exception if element is on the right of current customers
			int num = length - numberFromRight;
			if (num < 0) {
				throw new InvalidOperationException("Element unreachable!");
			}
				
			// Perform jumps and return
			while (num-- > 0) {
				customers = customers.Next;
			}
			return customers.Person;
        }
    }
}
