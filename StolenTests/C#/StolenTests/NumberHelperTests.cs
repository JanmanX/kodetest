﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;


namespace StolenTests
{
	[TestFixture]
    public class NumberHelperTests
    {
		// Generic test for the expected outcome
		// These are the examples from the README page.
		[Test]
		public void TestExpected()
		{
			var haystack = new List<int> { 1, 2, 3 };
			var helper = new NumberHelper ();

			// FindClosestNumbers(1,{1,2,3},1) returns 1
			List<int> l0 = new List<int> { 1 };
			List<int> closestNumbers = helper.FindClosestNumbers (1, haystack, 1);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));

			// FindClosestNumbers(6,{4,5},1) returns 5
			haystack = new List<int> { 4, 5 };
			l0 = new List<int> { 5 };
			closestNumbers = helper.FindClosestNumbers (6, haystack, 1);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));


			// FindClosestNumbers(11,{2,3,10},2) returns {10,3}
			haystack = new List<int> { 2, 3, 10 };
			l0 = new List<int> { 10, 3 };
			closestNumbers = helper.FindClosestNumbers (11, haystack, 2);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));
		}

		// Test the outcome, when 'null' is supplied instead of a haystack
        [Test]
        public void TestNullHaystack()
		{
			var helper = new NumberHelper ();

			List<int> l = helper.FindClosestNumbers (1, null, 1);
			Assert.AreEqual (0, l.Count);
		}

		// Test outcome of empty haystack (non-null)
		[Test]
		public void TestEmptyHaystack()
		{
			var haystack = new List<int> ();
			var helper = new NumberHelper ();

			List<int> l = helper.FindClosestNumbers (1, haystack, 1);
			Assert.AreEqual (0, l.Count);
		}

		// Test for no return, when n = 0
		[Test]
		public void TestZeroN()
		{
			var haystack = new List<int> { 1, 2, 3 };
			var helper = new NumberHelper ();

			List<int> l = helper.FindClosestNumbers (1, haystack, 0);
			Assert.AreEqual (0, l.Count);
		}

		// Test for no return, when n < 0
		[Test]
		public void TestNegativeN()
		{
			var haystack = new List<int> { 1, 2, 3 };
			var helper = new NumberHelper ();

			List<int> l = helper.FindClosestNumbers (1, haystack, int.MinValue);
			Assert.AreEqual (0, l.Count);
		}
			
		// Test when more needles than size of haystack is requested
		[Test]
		public void TestTooManyNeedles()
		{
			var haystack = new List<int> { 1, 2, 3 };
			var helper = new NumberHelper ();

			List<int> l = helper.FindClosestNumbers (1, haystack, int.MaxValue);
			Assert.AreEqual (haystack.Count, l.Count);
		}

		// Test that the needle is correctly floored
		[Test]
		public void TestRoundingFloor()
		{
			var haystack = new List<int> {int.MinValue, -3, -2, -1, 1, 2, 3, int.MaxValue };
			var helper = new NumberHelper ();

			// Test: Round from 0 to -1
			List<int> l0 = new List<int> { -1 };
			List<int> closestNumbers = helper.FindClosestNumbers (0, haystack, 1);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));

			// Test: Round from 0 to -1 and 1
			l0 = new List<int> { -1, 1 };
			closestNumbers = helper.FindClosestNumbers (0, haystack, 2);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));

			// Test: Round from 4 to 3:
			l0 = new List<int> { 3 };
			closestNumbers = helper.FindClosestNumbers (4, haystack, 1);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));

			// Test: Finding 4 nearest from int.MinValue
			l0 = new List<int> { int.MinValue, -3, -2, -1 };
			closestNumbers = helper.FindClosestNumbers (int.MinValue, haystack, 4);
			Assert.IsTrue(true, l0.SequenceEqual(closestNumbers));
		}
    }
}
