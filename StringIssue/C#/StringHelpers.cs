﻿using System.Collections.Generic;
using System.Text;

namespace StringIssue
{
    public static class StringHelpers
	{

		/// <summary>
		/// Method that does not perform well.
		/// </summary>
		/// <param name="strs"></param>
		/// <returns></returns>
		public static string MergeStrings(IEnumerable<string> strs)
		{
			int capacity = 0;
			foreach (string str in strs) {
				capacity += str.Length;
			}

			StringBuilder stringBuilder = new StringBuilder (capacity);

			foreach (string str in strs) {
				stringBuilder.Append (str);
			}
				
			// Return the string
			return stringBuilder.ToString();
		}
	}
}

/* Explain why your solution is faster below this line

In C#, 'string' type is immutable, meaning that the value of a 
string cannot be changed. When appending (+=) to a string, the
runtime actually has to allocate enough sequential memory, and
copy these two strings over. Not only is it computationally 
expensive to allocate and copy the strings, but it also adds a
lot of work for the Garbage Collector, which has to make
sure to clean-up afterwards.

Luckily, C# has a mutable string object called StringBuilder.
This object is specifically created for situations as these, where
multiple operations on strings are carried out. Amongst other
features, the StringBuilder lets the programmer define the required
capacity, so that object does not need to be moved in order to allocate
more bytes later.
This is utilized in out MergeStrings(...) method, where we first count
the total number of bytes required for the final string, by calculating
the sum of length of all the strings. Then, a StringBuilder is created
with this capacity, and the strings can be appended just as before,
without the need of allocating more memory along the way.

*/
