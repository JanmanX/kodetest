﻿using System;
using System.Collections.Generic;


namespace SecretAgent
{
    public class FindSecretAgent : IFindSecretAgent
    {
        public int StartSearch(IEnumerable<int> ids)
        {
            // YOUR SOLUTION GOES HERE
			//
			// This is a very usable function, that can be used in a wide variety of ways.
			// I find that the optimal solution for this depends on the application - be it for database-searches with
			// a large amount of IDs, or small but fast list-operations with few IDs.
			//
			// In this scenario, I chose the latter, where running-time is more important than space-complexity.
			//
			// Worst-case running time: O(n^2) 
			//     Sort      = O(n^2)    (Assuming worst case: https://msdn.microsoft.com/en-us/library/b0zbh7b6(v=vs.110).aspx)
			//	   Iteration = O(n)
			// 
			// Worst-case space complexity: O(n)
			//     New list of size 'n' is created = O(n)
			//

			// Create a list from the IEnumerable, so that we can sort it.
			// When sorted, the duplicates will be next to each other.
			List<int> myList = new List<int>(ids);
			myList.Sort ();

			// Iterate over list, check each value against the previous
			// The first iteration will never be true, as IDs range from 1 to 'n'
			int prev = 0;
			foreach (int i in myList) {
				// Check for duplicate
				if (prev == i) {
					return i;
				}

				// Store the value, proceed to next element
				prev = i;
			}

			// 0 is returned when no duplicate found.
			// Can be used as error code, as '0' is an invalid ID
			return 0;
        }
    }
}
